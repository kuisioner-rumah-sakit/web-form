export const getColorByGroupName = groupName => {
  switch (groupName) {
    case "Iklim Keselamatan":
      return ["#E6ECEB", 'black'];
    case "Perilaku Keselamatan":
      return ["#189AD3", 'black'];
    case "Maturitas":
      return ["#005073", "white"];
    default: 
      return ["#6AC9DB", "black"];
  }
}

export const isNetralFactor = factor => {
  return !([
    "Maturitas",
    "Mutu Rumah Sakit",
    "Keselamatan Pasien",
    "Keselamatan dan Kesehatan Pekerja"
  ].includes(factor))
}