function convertToCSV(objArray) {
  var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  var str = '';

  for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
          if (line !== '') line += ','

          line += array[i][index];
      }

      str += line + '\r\n';
  }

  return str;
}

export function exportCSVFile(items, fileTitle) {
  // change city province nya dlu
  const listRs = items.map(rs => ({
    nama: rs.nama,
    timestamp: rs.timestamp,
    tipe: rs.tipe,
    kelas: rs.kelas,
    kode: rs.kode,
    akreditasi: rs.akreditasi,
    tahun: rs.tahun,
    province: rs.province.name,
    city: rs.city.name,
    // iklim Keselamatan
    kerjasama : rs.kerjasama,
    komunikasi : rs.komunikasi,
    lingkungan: rs.lingkungan,
    pelatihan: rs.pelatihan,
    pelaporan: rs.pelaporan,
    pembelajaran: rs.pembelajaran,
    iklimKeselamatan: rs.iklimKeselamatan,
    // situasional
    regulasi: rs.regulasi,
    kepemimpinan: rs.kepemimpinan,
    risiko: rs.risiko,
    situasional: rs.situasional,
    // perilaku Keselamatan
    kepatuhan: rs.kepatuhan,
    partisipasi: rs.partisipasi,
    mengganggu: rs.mengganggu,
    perilakuKeselamatan: rs.perilakuKeselamatan,
    // maturitas
    mutu: rs.mutu,
    pasien: rs.pasien,
    pekerja: rs.pekerja,
    maturitas: rs.maturitas
  }));

  listRs.unshift({
    nama: "Nama Rumah Sakit",
    timestamp: "Tanggal Pengisian",
    tipe: "Jenis Rumah Sakit",
    kelas: "Kelas Rumah Sakit",
    kode: "Kode Rumah Sakit",
    akreditasi: "Akreditasi Kelulusan",
    tahun: "Tahun Kelulusan",
    province: "Provinsi",
    city: "KotaKabupaten",
    // iklim Keselamatan
    kerjasama : "Kerjasama Tim",
    komunikasi : "Komunikasi",
    lingkungan: "Lingkungan Kerja",
    pelatihan: "Pelatihan",
    pelaporan: "Pelaporan",
    pembelajaran: "Pembelajaran Organisasi",
    iklimKeselamatan: "Iklim Keselamatan",
    // situasional
    regulasi: "Regulasi",
    kepemimpinan: "Kepemimpinan",
    risiko: "Manajemen Risiko",
    situasional: "Situasional",
    // perilaku Keselamatan
    kepatuhan: "Kepatuhan Keselamatan",
    partisipasi: "Partisipasi Keselamatan",
    mengganggu: "Perilaku Mengganggu",
    perilakuKeselamatan: "Perilaku Keselamatan",
    // maturitas
    mutu: "Mutu Rumah Sakit",
    pasien: "Keselamatan Pasien",
    pekerja: "Keselamatan dan Kesehatan Pekerja",
    maturitas: "Maturitas Budaya Keselamatan"
  });

  // Convert Object to JSON
  var jsonObject = JSON.stringify(listRs);

  var csv = convertToCSV(jsonObject);

  var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

  var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
  if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
  } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
          // Browsers that support HTML5 download attribute
          var url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", exportedFilenmae);
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
      }
  }
}

// const headers = {
//   model: 'Phone Model'.replace(/,/g, ''), // remove commas to avoid errors
//   chargers: "Chargers",
//   cases: "Cases",
//   earphones: "Earphones"
// };

// const itemsNotFormatted = [
//   {
//       model: 'Samsung S7',
//       chargers: '55',
//       cases: '56',
//       earphones: '57',
//       scratched: '2'
//   },
//   {
//       model: 'Pixel XL',
//       chargers: '77',
//       cases: '78',
//       earphones: '79',
//       scratched: '4'
//   },
//   {
//       model: 'iPhone 7',
//       chargers: '88',
//       cases: '89',
//       earphones: '90',
//       scratched: '6'
//   }
// ];

// var itemsFormatted = [];

// // format the data
// itemsNotFormatted.forEach((item) => {
//   itemsFormatted.push({
//       model: item.model.replace(/,/g, ''), // remove commas to avoid errors,
//       chargers: item.chargers,
//       cases: item.cases,
//       earphones: item.earphones
//   });
// });

// var fileTitle = 'orders'; // or 'my-unique-title'

// exportCSVFile(headers, itemsFormatted, fileTitle); 