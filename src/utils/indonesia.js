import provinceList from '../resource/provinces.json';
import regencyList from '../resource/regencies.json';

function uCase(str) {
  return str.replace(/\w\S*/g, txt => {
    const exceptTxt = txt === 'DKI' || txt === 'DI' ? txt : txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    return exceptTxt
  })
}

const indonesia = {
  getAllProvinces: async () => {
    const provinces = await provinceList.map(province => {
      return {...province, name: uCase(province.name)};
    })
    return provinces
  },
  getRegenciesOfProvinceId: async provinceId => {
    if (!provinceId) return []
    const regencies = await regencyList
      .filter(regency => regency.province_id === provinceId)
      .map(regency => {
        return {...regency, name: uCase(regency.name)};
      })
    return regencies
  },
}

export default indonesia;