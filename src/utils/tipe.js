export default function getTipe(value) {
  if (value >= 8) return 'generatif';
  else if (value >= 6) return 'proaktif';
  else if (value >= 4) return 'birokratif';
  else if (value >= 2) return 'reaktif';
  else if (value >= 0) return 'patologik';
  else return null;
}

export const getStar = value => {
  // cek lagi ntar
  return Math.floor(value/2) + 1;
}

export const tipeColor = {
  generatif: '#D1AE59',
  proaktif: '#189AD3',
  birokratif: '#11A431',
  reaktif: '#FF0000',
  patologik: '#333333',
  netral: '#6F735C'
}
