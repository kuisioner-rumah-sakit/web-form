import axios from 'axios';

export const getPertanyaan = factorId => axios.get(
    `https://sheets.googleapis.com/v4/spreadsheets/1i8mLgUguKZ-ldCVGq36FYHsZhuH0uZc9yu3LuPRawyk/values/${factorId}!A:A?majorDimension=COLUMNS&key=AIzaSyCpDZtGEYCAv2MiqWNrpklSQUZoZVoii58`
  );

export const postResult = data => axios.post(`${process.env.REACT_APP_FIREBASE_DATABASE_URL_HASIL}`, data);

export const getAllRs = () => axios.get(`${process.env.REACT_APP_FIREBASE_DATABASE_URL_HASIL}`);