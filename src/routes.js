import Automation from "./views/Automation";
import Form from "./views/Form";
import Home from "./views/Home";
import Kuisioner from "./views/Kuisioner";
import Result from "./views/Result";
import ViewAll from "./views/ViewAll";
import ViewRs from "./views/ViewRs";

export default [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/form',
    exact: true,
    component: Form,
  },
  {
    path: '/form/:factorId',
    exact: true,
    component: Kuisioner,
  },
  {
    path: '/result',
    exact: true,
    component: Result,
  },
  {
    path: '/all',
    exact: true,
    component: ViewAll,
  },
  {
    path: '/histori',
    exact: true,
    component: ViewRs,
  },
  {
    path: '/typo',
    exact: true,
    component: Automation
  }
]