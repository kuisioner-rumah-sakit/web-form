import { createAsyncThunk } from "@reduxjs/toolkit";
import { getAllRs, getPertanyaan, postResult } from "../api";
import { postJawabanFormat } from "../calculation";

export const submitResult = createAsyncThunk(
  'finish/sendData', async ({profile, jawaban}) => {
    const data = {
      ...profile,
      ...postJawabanFormat(jawaban),
      timestamp: Date()
    }
    console.log(data)
    const response = await postResult(data);
    return response;
  }
)

export const getPertanyaanThunk = createAsyncThunk(
  'pertanyaan/getPertanyaan', async sheetId => {
    const response = await getPertanyaan(sheetId);
    return response;
  }
)

export const getAllRsThunk = createAsyncThunk(
  'viewall/getAll', async () => {
    const response = await getAllRs();
    return response;
  }
)