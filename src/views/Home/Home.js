import React from 'react';
import { Button, Grid } from '@material-ui/core';
import Fade from 'react-reveal/Fade';
import {ReactComponent as Illustration} from '../../assets/illustration.svg';
import bottomBg from '../../assets/bottom.png';
import InfoModal from '../../components/InfoModal';

const Home = props => {
  const { goToForm, goAllRs, goHasil } = props;
  return (
    <div style={{
        minHeight: '100vh'
      }}
    >
      <div style={{
        background: "linear-gradient(88.84deg, #6AC9DB 50.82%, rgba(106, 201, 219, 0) 112.94%)",
        width: "100%", minHeight: "4.5rem"
        }}
      />
      <Grid container style={{marginTop: "1rem"}} justify="center" align="center">
        <Grid item sm={4} style={{padding: "1rem", textAlign:"left", display: "flex"}}>
          <Fade left>
            <div>
              <h2 style={{color: "#051673", margin: 0}}>
                <span style={{fontSize: '1.5em'}}>DUTA-RS </span>
                (Dewasakan Upaya Tatanan Akreditasi Rumah Sakit)</h2>
              <p style={{color: "#212DB6", marginBottom: "2rem"}}>Untuk Mengukur Tingkat Kematangan Budaya Keselamatan Rumah Sakit</p>
              <Grid container justify="flex-end" style={{marginBottom:"1rem"}}>
                <InfoModal />
              </Grid>
              <Grid container justify="flex-end">
                <Button variant="contained" color="primary" onClick={goToForm}>Isi Data</Button>
              </Grid>
              <Grid container justify="flex-end" style={{marginTop: "1rem"}}>
                <Button
                  variant="contained"
                  color="default"
                  onClick={goAllRs}
                >
                  Cari Histori Pengisian RS
                </Button>
              </Grid>
              <Grid container justify="flex-end" style={{marginTop: "1rem"}}>
                <Button
                  variant="contained"
                  color="default"
                  onClick={goHasil}
                >
                  Lihat Hasil
                </Button>
              </Grid>
              <Grid container justify="flex-end" style={{marginTop: "1rem"}}>
                <a
                  href="https://docs.google.com/document/d/1_nOD1NdPS9wSf9_V05i2rCLe8Dd1y6y0C09f9ZBB94U/edit?usp=sharing"
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{
                    color: "blue",
                    textDecoration: 'none',
                    }}
                >Lihat Panduan Pengisian</a>
              </Grid>
            </div>
          </Fade>
        </Grid>
        <Grid item sm={6} style={{padding: "1rem"}}>
          <Fade>
            <Illustration width="100%" style={{maxWidth: "350px", height: "auto"}} />
          </Fade>
        </Grid>
      </Grid>
      <img src={bottomBg} alt="" style={{width: "100%", height: "auto", maxHeight: "9rem"}} />
    </div>
  )
};

export default Home;                                                                                                                                                                                
