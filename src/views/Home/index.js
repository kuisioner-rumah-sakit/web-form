import { connect } from 'react-redux';
import Home from './Home';
import { push } from 'connected-react-router';

// const mapStateToProps = state => ({
// })

const mapActionToProps = {
  goToForm: () => push("/form"),
  goAllRs: () => push("/histori"),
  goHasil: () => push("/result")
}

export default connect(null, mapActionToProps)(Home);