import React, { useState, useEffect } from 'react';
import { Button, Grid, MenuItem, TextField, withStyles } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import indonesia from '../../utils/indonesia';
import Fade from 'react-reveal/Fade';

const styles = () => ({
  root: {
    backgroundColor: "#DED4D4",
    minHeight: "100vh"
  },
  input: {
    backgroundColor: 'white',
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: "5px",
  },
  gridItem: {
    padding: "1rem"
  },
  helperText: {
    position: "absolute",
    top: "100%"
  }
});

const opsiKelas = ["A", "B", "C", "D"];
const opsiTipe = ["Umum"];
const opsiAkreditasi = ["Paripurna", "Utama", "Madya", "Dasar"];

const Form = ({classes, setProfile, profile, goNext}) => {
  const [provinceList, setProvinceList] = useState([]);
  const [cityList, setCityList] = useState([]);

  const [nama, setNama] = useState(profile.nama || "");
  const [tipe, setTipe] = useState(profile.tipe || "");
  const [kelas, setKelas] = useState(profile.kelas || "");
  const [kode, setKode] = useState(profile.kode || "");
  const [akreditasi, setAkreditasi] = useState(profile.akreditasi || "");
  const [tahun, setTahun] = useState(profile.tahun || 2020);
  const [province, setProvince] = useState(profile.province || null);
  const [city, setCity] = useState(profile.city || null);

  const [validation, setValidation] = useState(false);

  useEffect(() => {
    indonesia.getAllProvinces().then(res => setProvinceList(res));
    if(province) indonesia.getRegenciesOfProvinceId(province.id).then(res => setCityList(res));
  }, [province]);
  
  const handleChange = (set, e) => {
    set(e.target.value);
  }

  const handleNext = () => {
    setValidation(true);
    if (nama && tipe && kelas && kode && akreditasi && tahun && province && city) {
      setProfile({
        nama,
        tipe,
        kelas,
        kode,
        akreditasi,
        tahun,
        province,
        city
      });
      goNext();
    }
  }

  return (
    <div className={classes.root}>
    <Fade>
    <Grid container justify="center" style={{ padding: "1rem 2rem 4rem 2rem" }}>
      <Grid item justify="center" container>
        <h1 style={{color: "#005073"}}>Data Rumah Sakit</h1>
      </Grid>
      <Grid container style={{maxWidth: "700px"}}>
        <Grid item container className={classes.gridItem}>
          <TextField 
            label="Nama Rumah Sakit"
            variant="outlined"
            fullWidth
            size="small"
            value={nama}
            onChange={e => handleChange(setNama, e)}
            className={classes.input}
            error={ !nama && validation}
            helperText={ !nama && validation && "Harap Diisi"}
            FormHelperTextProps={{className: classes.helperText}}
          />
        </Grid>
        <Grid item container>
          <Grid item sm={6} xs={12} className={classes.gridItem}>
            <TextField
              select 
              label="Kelas Rumah Sakit"
              variant="outlined"
              fullWidth
              size="small"
              onChange={e => handleChange(setKelas, e)}
              className={classes.input}
              value={kelas}
              error={ !kelas && validation}
              helperText={ !kelas && validation && "Harap Diisi"}
              FormHelperTextProps={{className: classes.helperText}}
            >
              {opsiKelas.map(kls=>(
                <MenuItem key={kls} value={kls} selected={kelas===kls}>
                  Kelas {kls}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item sm={6} xs={12} className={classes.gridItem}>
            <TextField
              select
              label="Jenis Rumah Sakit"
              variant="outlined"
              fullWidth
              size="small"
              onChange={e => handleChange(setTipe, e)}
              className={classes.input}
              value={tipe}
              error={ !tipe && validation}
              helperText={ !tipe && validation && "Harap Diisi"}
              FormHelperTextProps={{className: classes.helperText}}
            >
              {opsiTipe.map(typ=>(
                <MenuItem key={typ} value={typ} selected={tipe===typ}>
                  {typ}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>
        <Grid item container>
          <Grid item sm={6} xs={12} className={classes.gridItem}>
            <TextField
              label="Kode Rumah Sakit"
              variant="outlined"
              fullWidth
              size="small"
              value={kode}
              onChange={e => handleChange(setKode, e)}
              className={classes.input}
              error={ !kode && validation}
              helperText={ !kode && validation && "Harap Diisi"}
              FormHelperTextProps={{className: classes.helperText}}
            />
          </Grid>
        </Grid>
        <Grid item container>
          <Grid item sm={8} xs={12} className={classes.gridItem}>
            <TextField
              select
              label="Status Kelulusan Akreditasi"
              variant="outlined"
              fullWidth
              size="small"
              value={akreditasi}
              onChange={e => handleChange(setAkreditasi, e)}
              className={classes.input}
              error={ !akreditasi && validation}
              helperText={ !akreditasi && validation && "Harap Diisi"}
              FormHelperTextProps={{className: classes.helperText}}
            >
              {opsiAkreditasi.map(akr=>(
                <MenuItem key={akr} value={akr} selected={akreditasi===akr}>
                  {akr}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item sm={4} xs={12} className={classes.gridItem}>
            <TextField 
              label="Tahun Kelulusan"
              variant="outlined"
              fullWidth
              size="small"
              type="number"
              value={tahun}
              onChange={e => handleChange(setTahun, e)}
              className={classes.input}
              error={ !tahun && validation}
              helperText={ !tahun && validation && "Harap Diisi"}
              FormHelperTextProps={{className: classes.helperText}}
            />
          </Grid>
        </Grid>
        <Grid item container>
          <Grid item sm={6} xs={12} className={classes.gridItem}>
            <Autocomplete 
              options={provinceList}
              getOptionLabel={x=>x.name}
              onChange={(_e, val)=>setProvince(val)}
              value={province}
              getOptionSelected={((x,y)=>x.id===y.id)}
              renderInput={props => (
                <TextField
                  {...props}
                  label="Provinsi"
                  variant="outlined"
                  fullWidth
                  size="small"
                  className={classes.input}
                  error={ !province && validation}
                  helperText={ !province && validation && "Harap Diisi"}
                  FormHelperTextProps={{className: classes.helperText}}
                />
              )}
            />
          </Grid>
          <Grid item sm={6} xs={12} className={classes.gridItem}>
            <Autocomplete 
              options={cityList}
              getOptionLabel={x=>x.name}
              disabled={!province}
              value={city}
              onChange={(_e, val)=>setCity(val)}
              getOptionSelected={((x,y)=>x.id===y.id)}
              renderInput={props => (
                <TextField
                  {...props}
                  label="Kota"
                  variant="outlined"
                  fullWidth
                  size="small"
                  className={classes.input}
                  disabled={!province}
                  error={ !city && validation}
                  helperText={ !city && validation && "Harap Diisi"}
                  FormHelperTextProps={{className: classes.helperText}}
                />
              )}
            />
          </Grid>
        </Grid>
        <Grid item container justify="flex-end" className={classes.gridItem}>
          <Button variant="contained" color="primary" onClick={handleNext}>Next</Button>
        </Grid>
      </Grid>
    </Grid>
    </Fade>
    </div>
  );
};

export default withStyles(styles)(Form);
