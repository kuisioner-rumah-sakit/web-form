import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { setProfile } from '../../store/form';
import Form from './Form';

const mapStateToProps = state => ({
  profile: state.form.profile,
});

const mapDispatchToProps = {
  setProfile,
  goNext: () => push("/form/kerjasama")
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);