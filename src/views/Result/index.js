import { push } from "connected-react-router";
import { connect } from "react-redux";
import { setDone } from "../../store/result";
import { getPertanyaanThunk, submitResult } from "../../thunk";
import Result from "./Result";

const mapStateToProps = state => ({
  formData: state.form,
  error: state.result.error,
  success: state.result.success,
  loading: state.result.loading,
  done: state.result.done,
  pertanyaan: state.pertanyaan.pertanyaan,
})
const mapDispatchToProps = {
  setDone,
  submitResult,
  getPertanyaanThunk,
  goHome: () => push('/'),
  goAllRs: () => push('/histori')
}
export default connect(mapStateToProps, mapDispatchToProps)(Result);