import React, { useEffect, useState } from 'react';
import { Button, Snackbar, withStyles } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { postJawabanFormat, validation } from '../../calculation';
import Hasil from '../../components/Hasil';
import style from './style';
import Variabel from '../../components/Variabel';
// import factors from '../../resource/factors.json';

const Result = ({
  formData,
  submitResult,
  loading,
  error,
  success,
  done,
  setDone,
  classes,
  // pertanyaan,
  // getPertanyaanThunk,
  goHome,
  goAllRs
}) => {
  const { profile, jawaban } = formData;
  useEffect(()=>{
    if (!(success || loading || error || done) && validation(formData))
      submitResult({ profile, jawaban });
  }, [success, loading, error, done, submitResult, profile, jawaban, formData]);

  // useEffect(() => {
  //   Object.keys(factors).forEach(factorId => {
  //     if (!pertanyaan[factorId] || pertanyaan[factorId] === [])
  //       getPertanyaanThunk(factorId.toUpperCase())
  //   })
  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  const [result] = useState(postJawabanFormat(jawaban));

  const handleCloseSuccess = (_event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setDone();
  }
  return (
    <>
    <Snackbar
      open={success}
      autoHideDuration={5000}
      onClose={handleCloseSuccess}
      anchorOrigin={{horizontal: 'center', vertical: 'top'}}
    >
      <Alert onClose={handleCloseSuccess} severity="success">
        Form Sukses dikirim! Terima Kasih telah mengisi
      </Alert>
    </Snackbar>
    <div className={classes.root}>
      {success && "SAVED!"}
      {error && "error"}
      {loading && "loading...."}
      {validation(formData) ? (
        <>
          <Hasil result={result} />
          <br />
          <br />
          <Variabel result={result} />
        </>
      ) : <h2 className={classes.blank}>Anda belum mengisi form untuk melihat hasil</h2>}
      <div className={classes.buttons}>
        <Button
          variant="contained"
          color="primary"
          onClick={goAllRs}
        >
          Lihat Histori Pengisian RS
        </Button>
        <Button
          variant="contained"
          color="default"
          onClick={goHome}
        >
          Back To Home
        </Button>
      </div>
    </div>
    </>
  );
}

export default withStyles(style)(Result);