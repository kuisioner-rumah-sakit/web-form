export default () => ({
  root: {
    backgroundColor: '#DED4D4',
    minHeight: '100vh',
    padding: '2rem'
  },
  buttons: {
    margin: '3rem auto 1rem',
    display: 'flex',
    justifyContent: 'space-between',
    maxWidth: 900
  },
  blank: {
    textAlign: "center",
    margin: "2rem"
  }
})