import { capitalize, Collapse, Grid, TableCell, TableRow } from '@material-ui/core';
import React, { useState } from 'react';
import getTipe, { tipeColor } from '../../utils/tipe';
import factorGroup from '../../resource/factorGroup.json';
import { isNetralFactor } from '../../utils/factor';

const RowItem = ({classes, rs, index}) => {
  const {
    nama,
    tipe,
    kelas,
    kode,
    akreditasi,
    tahun,
    province,
    city,
    timestamp,
    // hasil
    // // iklim Keselamatan
    // kerjasama,
    // komunikasi,
    // lingkungan,
    // pelatihan,
    // pelaporan,
    // pembelajaran,
    // // situasional
    // regulasi,
    // kepemimpinan,
    // risiko,
    // // perilaku Keselamatan
    // kepatuhan,
    // partisipasi,
    // mengganggu,
    // // maturitas
    // mutu,
    // pasien,
    // pekerja,
    // // groupFactor
    // iklimKeselamatan,
    // situasional,
    // perilakuKeselamatan,
    maturitas,
  } = rs;
  const tingkatKematangan = getTipe(maturitas) || "";
  const [open, setOpen] = useState(false);
  return (
    <>
      <TableRow className={classes.row} key={`${nama}-${index}-main`} onClick={()=>setOpen(!open)}>
        <TableCell align="center" className={classes.bold}>{index+1}</TableCell>
        <TableCell align="center">{nama}</TableCell>
        <TableCell align="center">{(timestamp || "").substr(4,20)}</TableCell>
        <TableCell align="center">{tipe}</TableCell>
        <TableCell align="center">{kode}</TableCell>
        <TableCell align="center">{kelas}</TableCell>
        <TableCell align="center">{akreditasi}</TableCell>
        <TableCell align="center">{tahun}</TableCell>
        <TableCell align="center">{province.name}</TableCell>
        <TableCell align="center">{city.name}</TableCell>
        <TableCell align="center">
          <span style={{
            backgroundColor: tipeColor[tingkatKematangan],
            borderRadius: 10,
            padding: "0.5rem 0.67rem",
            color: '#fff'
          }}>
            {capitalize(tingkatKematangan)}
          </span>
        </TableCell>
      </TableRow>
      <TableRow className={classes.collapseRow} key={`${nama}-${index}-collapse`}>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={11}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Grid container className={classes.collapseContainer}>
              {factorGroup.map(group => (
                <Grid item container xs={12}>
                  <Grid item xs={3}>
                    <h3>{group.name}</h3>
                  </Grid>
                  <Grid item xs={9} container alignItems="center">
                    <span
                      style={{
                        backgroundColor: isNetralFactor(group.name) ? tipeColor.netral : tipeColor[getTipe(rs[group.id])],
                        borderRadius: "50px",
                        textAlign: "center",
                        padding: "0.5rem 0.67rem",
                        color: '#fff',
                        minWidth: "4rem",
                        fontWeight: 600,
                        fontSize: "1.2em"
                      }}
                    >{Math.round(rs[group.id]*10)/10}</span>
                  </Grid>
                  {group.factors.map(factor => (
                    <Grid container>
                      <Grid item xs={4} container alignItems="center">
                        <p style={{ marginLeft: "1rem" }}>{factor.name}</p>
                      </Grid>
                      <Grid item xs={8} container alignItems="center">
                        <span
                          style={{
                            backgroundColor: isNetralFactor(factor.name) ? tipeColor.netral : tipeColor[getTipe(rs[factor.id.toLowerCase()])],
                            borderRadius: "50px",
                            textAlign: "center",
                            padding: "0.5rem 0.67rem",
                            color: '#fff',
                            minWidth: "4rem"
                          }}
                        >
                          {Math.round(rs[factor.id.toLowerCase()]*10)/10}
                        </span>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              ))}
            </Grid>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
}

export default RowItem;