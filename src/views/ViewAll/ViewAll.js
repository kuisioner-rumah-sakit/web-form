import React, { useEffect, useState } from 'react';
import {
  Table,
  TableRow,
  TableHead,
  TableCell,
  Grid,
  withStyles,
  TableContainer,
  TableBody,
  Button,
} from '@material-ui/core';
import SearchField from '../../components/SearchField';
import RowItem from './RowItem';
import ColorInfo from '../../components/Hasil/ColorInfo';
import { exportCSVFile } from '../../utils/csv';

const style = (theme) => ({
  table: {
    borderCollapse: "separate",
    borderSpacing: "0 1rem" 
  },
  bold: { 
    fontWeight: 600,
  },
  headRow: {
    backgroundColor: "#E6ECEB",
    '&>th': {
      fontFamily: "inherit",
      fontWeight: 500,
      border: "none",
    },
    '&>th:first-child': { borderRadius: "10px 0 0 10px" },
    '&>th:last-child': { borderRadius: "0 10px 10px 0" }
  },
  container: {
    overflowX: "auto"
  },
  row: {
    backgroundColor: "#fff",
    cursor: "pointer",
    '&>td': {
      fontFamily: "inherit",
      border: "none"
    },
    '&>td:first-child': { borderRadius: "10px 0 0 10px" },
    '&>td:last-child': { borderRadius: "0 10px 10px 0" }
  },
  collapseRow: {
    backgroundColor: "#E6ECEB",
    transform: "translateY(calc(-1rem - 10px))",
    '&>td': {
      fontFamily: "inherit",
      border: "none",
      borderRadius: "0 0 10px 10px"
    }
  },
  collapseContainer: {
    padding: "2rem 1rem"
  },
  infoContainer: {
    maxWidth: 900,
    margin: '1rem auto 0'
  },
});

const ViewAll = ({classes, getAllRs, allRs}) => {
  useEffect(() => {
    getAllRs();
  }, [getAllRs]);

  const [search, setSearch] = useState("");

  return (
    <div
      style={{
        backgroundColor: "#DED4D4",
        minHeight: "100vh",
        padding: "3rem 2rem"
      }}
    >
      <Grid style={{marginBottom: "3rem"}}>
        <h1 style={{fontWeight: 700, margin: 0}}>
          Daftar Rumah Sakit
        </h1>
        <h4 style={{fontWeight: 500, margin: "0.5rem 0" }}>Yang Sudah Mengisi</h4>
      </Grid>
      <Grid container justify="flex-end">
        <Button
          variant="contained"
          color="primary"
          style={{margin: "1rem"}}
          onClick={() => exportCSVFile(allRs, "Data-Rumah-Sakit")}
        >
          Extract
        </Button>
      </Grid>
      <Grid container>
        <SearchField 
          placeholder="Cari Berdasarkan Kode Rumah Sakit"
          value={search}
          onChange={setSearch}
        />
      </Grid>
      <Grid container justify="space-between" className={classes.infoContainer}>
        <ColorInfo tipe="patologik" />
        <ColorInfo tipe="reaktif" />
        <ColorInfo tipe="birokratif" />
        <ColorInfo tipe="proaktif" />
        <ColorInfo tipe="generatif" />
      </Grid>
      <TableContainer className={classes.container}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow className={classes.headRow}>
              <TableCell align="center">No</TableCell>
              <TableCell align="center">Nama Rumah Sakit</TableCell>
              <TableCell align="center">Tanggal Pengisian</TableCell>
              <TableCell align="center">Tipe Rumah Sakit</TableCell>
              <TableCell align="center">Kode Rumah Sakit</TableCell>
              <TableCell align="center">Kelas</TableCell>
              <TableCell align="center">Status Kelulusan</TableCell>
              <TableCell align="center">Tahun Kelulusan</TableCell>
              <TableCell align="center">Provinsi</TableCell>
              <TableCell align="center">Kota/Kabupaten</TableCell>
              <TableCell align="center">Tingkat Kematangan</TableCell>
            </TableRow>
            <TableRow />
          </TableHead>
          <TableBody>
            {allRs.filter(x=>String(x.kode).match(search)).map((rs, i) => (
              <RowItem
                rs={rs}
                index={i}
                classes={classes}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
};

export default withStyles(style)(ViewAll);