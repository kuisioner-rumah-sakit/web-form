import React, { useEffect, useRef, useState } from 'react';
import { Button, FormHelperText, Grid, withStyles } from '@material-ui/core';
import Soal from '../../components/Soal';
import factors from '../../resource/factors.json';

const style = () => ({
  root: {
    padding: "3rem 2rem",
    color: "#111"
  },
  header: {
    backgroundColor: "#6AC9DB",
    padding: "2rem 0",
    margin: "1rem",
    borderRadius: 8
  },
  subheader: {
    backgroundColor: "#B2F9FE",
    padding: "1rem",
    margin: "1rem 0",
    borderRadius: 5
  },
  error: {
    width: "100%",
    textAlign: "center"
  },
});

const Kuisioner = props => {
  const {
    classes,
    getPertanyaanThunk,
    pertanyaan,
    match,
    push,
    setForm,
    jawaban,
    resetDone,
  } = props;
  const { params: { factorId }} = match;
  const listPertanyaan = pertanyaan[factorId];
  const [listJawaban, setListJawaban] = useState(jawaban[factorId]);
  const factor = factors[factorId];
  const topRef = useRef();
  const [error, setError] = useState(false);

  useEffect(() => {
    setListJawaban(jawaban[factorId]);
  }, [factorId, jawaban]);

  useEffect(() => {
    if(factor) getPertanyaanThunk(factor.id);
  }, [factor, getPertanyaanThunk]);

  useEffect(() => {
    setError(false);
  }, [listJawaban])

  const validation = () => {
    for (let jwb of listJawaban) {
      if (jwb === null || jwb === undefined) {
        return false;
      }
    }
    return true;
    // for (let jwb of listJawaban) {
    //   if (jwb !== null && jwb !== undefined) {
    //     return true;
    //   }
    // }
    // return false;  // nanti di akhir diganti yg bener
  }

  const handleNext = () => {
    if (validation()) {
      topRef.current.scrollIntoView({behavior: 'smooth'});
      setForm({factorId, data: listJawaban});
      if (factor.next === 'finish') {
        resetDone();
        push('/result');
      }
      else push(`/form/${factor.next}`);
    } else {
      setError(true);
    }
  }

  const handlePrev = () => {
    topRef.current.scrollIntoView({behavior: 'smooth'});
    setForm({factorId, data: listJawaban});
    push(`/form/${factor.prev}`);
  }

  const handleSetJawaban = (index, value) => {
    const newListJawaban = Array.from(listJawaban);
    newListJawaban[index] = value;
    setListJawaban(Array.from(newListJawaban));
  }

  return (
    <div ref={topRef} >
      {factor &&
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <div className={classes.header}>
              <h1 style={{textAlign: "center"}}>{factor.groupName}</h1>
            </div>
          </Grid>
          <Grid item container xs={12}>
            <div className={classes.subheader}>
              <h3 style={{textAlign: "center", margin: 0}}>
                {`${factor.number} ${factor.name} (${listPertanyaan ? listPertanyaan.length : '-'} Elemen Penilaian)`}
              </h3>
            </div>
          </Grid>
          <Grid item xs={12}>
            {(listPertanyaan && listJawaban) ? listPertanyaan.map((p, i) => 
              <Soal
                pertanyaan={p}
                index={i}
                key={`${factorId}-${i}`}
                jawaban={listJawaban[i]}
                setJawaban={handleSetJawaban}
              />
            ) : [...Array(3)].map((_x, i) => <Soal dummy index={i} key={i} />)}
          </Grid>
          {error && <FormHelperText error className={classes.error}>Anda Belum Mengisi Seluruh Pertanyaan di Halaman ini</FormHelperText>}
          <Grid item container justify="space-between" xs={12} style={{margin: "2rem 0"}}>
            <Button variant="contained" color="default" style={{width: "100px"}} onClick={handlePrev}>Prev</Button>
            <Button variant="contained" color="primary" style={{width: "100px"}} onClick={handleNext}>Next</Button>
          </Grid>
        </Grid>
      }
    </div>
  );
}

export default withStyles(style)(Kuisioner);