import { connect } from "react-redux";
import Kuisioner from "./Kuisioner";
import { getPertanyaanThunk } from "../../thunk";
import { push } from "connected-react-router";
import { setForm } from "../../store/form";
import { resetDone } from "../../store/result";

const mapStateToProps = state => ({
  pertanyaan: state.pertanyaan.pertanyaan,
  jawaban: state.form.jawaban,
  loading: state.pertanyaan.loading,
  error: state.pertanyaan.error
})

const mapDispatchToProps = {
  push,
  setForm,
  getPertanyaanThunk,
  resetDone
}
export default connect(mapStateToProps, mapDispatchToProps)(Kuisioner);