import { connect } from 'react-redux';
import { setForm } from "../../store/form";
import Automation from './Automation';

const mapStateToProps = state => ({
});

const mapActionToProps = {
  setForm
}

export default connect(mapStateToProps, mapActionToProps)(Automation);