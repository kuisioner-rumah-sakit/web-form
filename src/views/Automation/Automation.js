import React from 'react';
import { useState } from 'react';
import factors from '../../resource/factors.json';

const Automation = ({setForm}) => {
  const [data, setData] = useState({});

  const handleChange = (e, factor) => {
    const raw = e.target.value;
    const autoData = raw.split("	").map(x=>x/1);
    console.log(autoData);
    setData({...data, [factor]: autoData});
  }

  const save = () => {
    Object.keys(data).forEach(factor => {
      setForm({factorId: factor, data: data[factor] });
    })
  }

  return (
    <div>
      {Object.keys(factors).map(o => (
        <div>
          <h6>{o}</h6>
          <input type="text" onChange={e => handleChange(e, o)} />
          <br />
        </div>
      ))}
      <button onClick={save}>SAVE</button>
    </div>
  );
}

export default Automation;