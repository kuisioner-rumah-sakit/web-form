import ViewRs from "./VIewRs";
import { connect } from "react-redux";
import { getAllRsThunk } from '../../thunk';

const mapStateToProps = state => ({
  allRs: state.viewall.allRs,
  loading: state.viewall.loading,
  error: state.viewall.error,
});

const mapDispatchToProps = {
  getAllRs: getAllRsThunk
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewRs);