import { createSlice } from "@reduxjs/toolkit";
import { getPertanyaanThunk } from "../../thunk";

const pertanyaanSlice = createSlice({
  name: 'pertanyaan',
  initialState: {
    loading: false,
    error: null,
    pertanyaan: {}
  },
  extraReducers: {
    [getPertanyaanThunk.pending]: state => {
      state.loading = true;
    },
    [getPertanyaanThunk.fulfilled]: (state, action) => {
      state.loading = false;
      const dataPertanyaan = action.payload.data.values[0];
      const id = dataPertanyaan[0];
      const pertanyaan = dataPertanyaan.slice(1);
      state.pertanyaan[id] = pertanyaan;
    },
    [getPertanyaanThunk.rejected]: state => {
      state.loading = false;
    },
  }
})

const { reducer } = pertanyaanSlice;

export default reducer;