import { createSlice } from "@reduxjs/toolkit";
import { submitResult } from "../../thunk";

const resultSlice = createSlice({
  name: 'result',
  initialState: {
    loading: false,
    error: null,
    success: false,
    done: false,
  },
  reducers: {
    setDone(state) {
      state.loading = false;
      state.error = null;
      state.success = false;
      state.done = true;
    },
    resetDone(state) {
      state.done = false;
    }
  },
  extraReducers: {
    [submitResult.pending]: state => {
      state.loading = true;
      state.error = null;
      state.success = false;
    },
    [submitResult.fulfilled]: (state, _action) => {
      state.loading = false;
      state.success = true;
    },
    [submitResult.rejected]: state => {
      state.loading = false;
    },
  }
})

const { reducer, actions } = resultSlice;

export const { setDone, resetDone } = actions;

export default reducer;