import { createSlice } from "@reduxjs/toolkit";

const formSlice = createSlice({
  name: 'general',
  initialState: {
    firstTime: true
  },
  reducers: {
    setFirstTime(state, action) {
      state.firstTime = action.payload;
    }
  },
})

// Extract the action creators object and the reducer
const { actions, reducer } = formSlice;
// Extract and export each action creator by name
export const { setFirstTime } = actions;
// Export the reducer, either as a default or named export
export default reducer;