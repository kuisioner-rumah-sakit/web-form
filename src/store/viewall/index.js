import { createSlice } from "@reduxjs/toolkit";
import { getAllRsThunk } from "../../thunk";

const resultSlice = createSlice({
  name: 'viewall',
  initialState: {
    loading: false,
    error: null,
    allRs: []
  },
  extraReducers: {
    [getAllRsThunk.pending]: state => {
      state.loading = true;
    },
    [getAllRsThunk.fulfilled]: (state, action) => {
      state.loading = false;
      const { data } = action.payload;
      state.allRs = Object.values(data).reverse();
    },
    [getAllRsThunk.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
  }
})

const { reducer, actions } = resultSlice;

export const { setDone } = actions;

export default reducer;