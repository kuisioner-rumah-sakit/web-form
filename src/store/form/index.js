import { createSlice } from "@reduxjs/toolkit";
import { getPertanyaanThunk } from "../../thunk";

const formSlice = createSlice({
  name: 'form',
  initialState: {
    profile: {},
    jawaban: {}
  },
  reducers: {
    setProfile(state, action) {
      state.profile = action.payload;
    },
    setForm(state, action) {
      const { factorId, data } = action.payload;
      state.jawaban[factorId] = data;
    }
  },
  extraReducers: {
    [getPertanyaanThunk.fulfilled]: (state, action) => {
      const dataPertanyaan = action.payload.data.values[0];
      const id = dataPertanyaan[0];
      const pertanyaanCount = dataPertanyaan.slice(1).length;
      if (!state.jawaban[id])
        state.jawaban[id] = [...Array(pertanyaanCount)];
    },
  }
})

// Extract the action creators object and the reducer
const { actions, reducer } = formSlice;
// Extract and export each action creator by name
export const { setProfile, setForm } = actions;
// Export the reducer, either as a default or named export
export default reducer;