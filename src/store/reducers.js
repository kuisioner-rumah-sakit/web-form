import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { getPersistedReducer } from './persist';
import pertanyaan from './pertanyaan';
import result from './result';
import form from './form';
import viewall from './viewall';
import general from './general';

const createRootReducer = history => combineReducers({
  router: connectRouter(history),
  pertanyaan,
  general: getPersistedReducer('general', general),
  result: getPersistedReducer('result', result, ['done']),
  form: getPersistedReducer('form', form),
  viewall,
})

export default createRootReducer;