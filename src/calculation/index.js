import factors from '../resource/factors.json';
import factorGroup from '../resource/factorGroup.json';

export const averageJawaban = jawaban => {
  const result = {};
  Object.keys(factors).forEach(factorId => {
    const factorAnswers = jawaban[factorId];
    if (factorAnswers) {
      let sum = 0;
      factorAnswers.forEach(x => sum += (x || 0));
      result[factorId] = sum / factorAnswers.length;
    }
  });
  return result;
}

export const toList = avgObj => {
  return Object.keys(avgObj).map(factorId => ({
    nama: factors[factorId].name,
    avg: avgObj[factorId]
  }))
}

export const averageGroup = averagedJawaban => {
  const result = {}
  factorGroup.forEach(grp=>{
    let groupCount = 0;
    grp.factors.forEach(factor => {
      groupCount += (averagedJawaban[factor.id.toLowerCase()] || 0);
    })
    result[grp.id] = groupCount / grp.factors.length; 
  })
  return result;
}

export const postJawabanFormat = jawaban => {
  const jawabanAveraged = averageJawaban(jawaban);
  const groupAveraged = averageGroup(jawabanAveraged);
  return {...jawabanAveraged, ...groupAveraged};
}

export const validation = ({profile, jawaban}) => {
  if (!profile.nama) return false;
  
  const averagedJawaban = averageJawaban(jawaban);
  for (let factorId of Object.keys(factors)) {
    const value = averagedJawaban[factorId];
    if (value === null || value === undefined) return false;
  }
  return true;
}

export const getBottom3 = resultObj => {
  const resultList = toList(resultObj);
  resultList.sort((x,y) => (x.avg-y.avg));
  return resultList.slice(0,3);
}