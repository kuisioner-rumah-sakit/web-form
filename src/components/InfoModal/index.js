import { connect } from "react-redux";
import { setFirstTime } from "../../store/general";
import InfoModal from "./InfoModal";

const mapStateToProps = state => ({
  firstTime: state.general.firstTime
})

const mapActionToProps = {
  setFirstTime,
}


export default connect(mapStateToProps, mapActionToProps)(InfoModal);