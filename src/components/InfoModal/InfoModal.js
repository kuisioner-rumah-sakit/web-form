import React, { useState } from 'react';
import { Dialog, DialogContent, DialogTitle, Grid } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

const InfoModal = ({firstTime, setFirstTime}) => {

  console.log(firstTime, setFirstTime)
  const [open, setOpen] = useState(firstTime);
  const handleClose = () => {
    setFirstTime(false);
    setOpen(false);
  };
  
  return (
    <>
      <Dialog
        onClose={handleClose}
        open={open}
        aria-labelledby="Result-dialog-infooo"
        fullWidth
        maxWidth="xs"
      >
        <DialogTitle style={{padding: '1.5rem 1rem 0.5rem'}}>
          <h2 style={{margin: "0 1rem", color: "#051673"}}>Notifikasi</h2>
          <IconButton
            aria-label="close"
            style={{
              position: 'absolute',
              right: "1rem",
              top: "1rem",
              color: "#c1c1c1",
            }}
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <ol style={{marginTop: "0", paddingLeft: "1rem"}}>
              <li style={{margin: "0.8rem 0 0.8rem 0.8rem"}}>Website DUTA-RS ditujukan untuk rumah sakit Umum</li>
              <li style={{margin: "0.8rem 0 0.8rem 0.8rem"}}>Website DUTA-RS diisi oleh asesor internal rumah sakit yang sudah mengikuti pelatihan</li>

            </ol>
          </Grid>
        </DialogContent>
      </Dialog>
      <span
        onClick={() => setOpen(true)}
        style={{
          cursor: 'pointer',
          fontSize: '1em',
          color: 'blue',
        }}
      >Notifikasi</span>
    </>
  )
}

export default InfoModal;
