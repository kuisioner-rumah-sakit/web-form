import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import { InputBase, withStyles } from '@material-ui/core';

const style = (theme) => ({
  search: {
    position: 'relative',
    borderRadius: 10,
    backgroundColor: "#D1D1D1",
    fontFamily: 'inherit',
    '&:hover': {
      backgroundColor: "#CCCCCC",
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '40%',
      minWidth: 360
    },
  },
  searchIcon: {
    padding: "0 1rem",
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    fontFamily: 'inherit',
    width: "90%",
  },
  inputInput: {
    padding: theme.spacing(1.5, 1, 1.5, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + 2rem)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    fontSize: "0.9em"
  },
})

const SearchField = ({classes, placeholder, onChange, value}) => (
  <div className={classes.search}>
    <div className={classes.searchIcon}>
      <SearchIcon />
    </div>
    <InputBase
      placeholder={placeholder || "Search..."}
      onChange={e => onChange(e.target.value)}
      value={value}
      classes={{
        root: classes.inputRoot,
        input: classes.inputInput,
      }}
      inputProps={{ 'aria-label': 'search' }}
    />
  </div>
);

export default withStyles(style)(SearchField);