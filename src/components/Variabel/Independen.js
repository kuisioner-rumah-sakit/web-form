import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import ResultBar from '../ResultBar';
import ResultDialogs from '../ResultDialog';

const Independen = ({result}) => {
  const {
    // iklim Keselamatan
    kerjasama,
    komunikasi,
    lingkungan,
    pelatihan,
    pelaporan,
    pembelajaran,
    // situasional
    regulasi,
    kepemimpinan,
    risiko,
    // perilaku Keselamatan
    kepatuhan,
    partisipasi,
    mengganggu,
    // groupFactor
    iklimKeselamatan,
    situasional,
    perilakuKeselamatan,
  } = result;
  const lowestValue = Math.min(...[iklimKeselamatan, situasional, perilakuKeselamatan]);
  const lowestFactor = () => {
    switch (lowestValue) {
      case iklimKeselamatan:
        return "Iklim Keselamatan";
      case situasional:
        return "Situasional";
      case perilakuKeselamatan: 
        return "Perilaku Keselamatan";
      default:
        return "Tidak Ada";
    }
  }
  const [openDialog, setOpenDialog] = useState(0);
  const closeDialog = () => setOpenDialog(0);

  return (
    <Grid container style={{ padding: "0 1rem 1rem" }}>
      <ResultBar factor="Iklim Keselamatan" value={iklimKeselamatan} openDialog={()=>setOpenDialog(1)} />
      <ResultBar factor="Situasional" value={situasional} openDialog={()=>setOpenDialog(2)}/>
      <ResultBar factor="Perilaku Keselamatan" value={perilakuKeselamatan} openDialog={()=>setOpenDialog(3)}/>
      <Grid item xs={12}>
        <h2 style={{fontWeight: 300, textAlign: "center", width: "85%", margin: "2rem auto 0"}}>
          Variabel Terlemah adalah <span style={{fontWeight: 500}}>{lowestFactor()}</span>
        </h2>
      </Grid> 
      <ResultDialogs
        open={openDialog===1}
        handleClose={closeDialog}
        variabel="Iklim Keselamatan"
        result={{kerjasama,komunikasi, lingkungan, pelatihan, pembelajaran, pelaporan}}
      />
      <ResultDialogs
        open={openDialog===2}
        handleClose={closeDialog}
        variabel="Situasional"
        result={{regulasi, kepemimpinan, risiko}}
      />
      <ResultDialogs
        open={openDialog===3}
        handleClose={closeDialog}
        variabel="Perilaku Keselamatan"
        result={{kepatuhan, partisipasi, mengganggu}}
      />
    </Grid>
  );
}

export default Independen;