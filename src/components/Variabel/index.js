import React, { useState } from 'react';
import { Grid, Paper, withStyles } from '@material-ui/core';
import CustomTab from '../CustomTab';
import CustomTabs from '../CustomTabs';
import Dependen from './Dependen';
import Independen from './Independen';

const styles = () => ({
  root: {
    padding: '1rem 1.5rem'
  },
  title: {
    textShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
    fontWeight: 500,
    textAlign: 'center',
    margin: 0
  },
  grid: {
    marginTop: '1rem'
  },
  paper: {
    borderRadius: 30,
    maxWidth: 900,
    margin: 'auto'
  }
});

const Variabel = ({result, classes}) => {

  const [tab, changeTab] = useState(0);
  return (
    <>
      <CustomTabs
        value={tab}
        indicatorColor="primary"
        textColor="primary"
        aria-label="disabled tabs example"
        onChange={(_e, val)=>changeTab(val)}
      >
        <CustomTab label="Variabel Kematangan Budaya Keselamatan"></CustomTab>
        <CustomTab label="Variabel Budaya Keselamatan"></CustomTab>
      </CustomTabs>
      <Paper className={classes.paper}>
        <Grid container className={classes.root}>
          {tab === 0 && <Dependen result={result}/>}
          {tab === 1 && <Independen result={result}/>}
        </Grid>
      </Paper>
    </>
  );
}

export default withStyles(styles)(Variabel);