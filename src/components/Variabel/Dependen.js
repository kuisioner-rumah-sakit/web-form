import React from 'react';
import { Grid } from '@material-ui/core';
import ResultBar from '../ResultBar';
import ModalPerbaikan from '../ModalPerbaikan';

const Dependen = ({result}) => {
  const lowestValue = Math.min(...[result.mutu, result.pasien, result.pekerja]);
  const lowestFactor = () => {
    switch (lowestValue) {
      case result.mutu:
        return "Mutu Rumah Sakit";
      case result.pasien:
        return "Keselamatan Pasien";
      case result.pekerja: 
        return "Keselamatan dan Kesehatan Pekerja";
      default:
        return "Tidak Ada";
    }
  }

  return (
    <Grid container style={{ padding: "0 1rem 1rem" }}>
      
      <ModalPerbaikan factorId="mutu" />
      <ResultBar factor="Mutu Rumah Sakit" value={result.mutu} />

      <ModalPerbaikan factorId="pasien" />
      <ResultBar factor="Keselamatan Pasien" value={result.pasien} />

      <ModalPerbaikan factorId="pekerja" />
      <ResultBar factor="Keselamatan dan Kesehatan Pekerja" value={result.pekerja} />

      <Grid item xs={12}>
        <h2 style={{fontWeight: 300,
          textAlign: "center",
        width: "85%",
        margin: "2rem auto 0"}}>
          Indikator Terlemah
          adalah <span style={{fontWeight: 500}}>{lowestFactor()}</span>
        </h2>
      </Grid> 
    </Grid>
  );
}

export default Dependen;