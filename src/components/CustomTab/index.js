import React from 'react';
import { Tab, withStyles } from '@material-ui/core';

const CustomTab = withStyles({
  root: {
    color: '#fff !important',
    borderRadius: '10px 10px 0 0',
    backgroundColor: '#969696',
    textTransform: "none",
    fontSize: '1rem',
    maxWidth: '50%',
    boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)',
    fontFamily: "inherit"
  },
  selected: {
    color: '#333 !important',
    backgroundColor: 'white',
  },
})((props) => <Tab disableRipple {...props}/>);

export default CustomTab;