import { connect } from "react-redux";
import { getPertanyaanThunk } from "../../thunk";
import ModalPerbaikan from "./ModalPerbaikan";

const mapStateToProps = state => ({
  pertanyaan: state.pertanyaan.pertanyaan,
  jawaban: state.form.jawaban
});

const mapActionToProps = {
  getPertanyaan: getPertanyaanThunk
}

export default connect(mapStateToProps, mapActionToProps)(ModalPerbaikan);