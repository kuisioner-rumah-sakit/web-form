import React, { useEffect, useState } from 'react';
import { Dialog, DialogContent, DialogTitle, Grid } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import factors from '../../resource/factors.json';

const ModalPerbaikan = ({pertanyaan, jawaban, factorId, getPertanyaan}) => {
  const pertanyaanFactor = pertanyaan && pertanyaan[factorId];
  const jawabanFactor = jawaban && jawaban[factorId];
  useEffect(() => {
    if(!pertanyaanFactor || pertanyaanFactor === []) getPertanyaan(factorId.toUpperCase());
  }, [pertanyaanFactor, getPertanyaan, factorId]);

  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);
  
  return (
    <>
    <Dialog
      onClose={handleClose}
      open={open}
      aria-labelledby="Result-dialog-perbaikan"
      fullWidth
      maxWidth="md"
    >
      <DialogTitle style={{padding: '1.5rem 1rem 0.5rem'}}>
        <h3>Saran Upaya Perbaikan Indikator {factors[factorId].name}</h3>
        <IconButton
          aria-label="close"
          style={{
            position: 'absolute',
            right: "1rem",
            top: "1rem",
            color: "#c1c1c1",
          }}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item xs={12}>
            <h4>Sangat Direkomendasikan <span>(masih bernilai 0)</span></h4>
            {pertanyaanFactor && pertanyaanFactor.map((p, i) => {
              if(jawabanFactor[i] === 0)
                return (<p>{p}</p>);
              else
                return null;
            })}
          </Grid>
          <Grid item xs={12}>
            <h4>Direkomendasikan <span>(masih bernilai 5)</span></h4>
            {pertanyaanFactor && pertanyaanFactor.map((p, i) => {
              if(jawabanFactor[i] === 5)
                return (<p>{p}</p>);
              else
                return null;
            })}
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
    <span
        onClick={() => setOpen(true)}
        style={{
          cursor: 'pointer',
          fontSize: '0.9em',
          color: 'blue',
          '&:hover': {textDecoration: 'underline'}
        }}
      >Saran Perbaikan</span>
    </>
  )
}

export default ModalPerbaikan;