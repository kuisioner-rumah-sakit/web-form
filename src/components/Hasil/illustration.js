import React from 'react';
import {ReactComponent as Generatif} from '../../assets/generatif.svg';
import {ReactComponent as Proaktif} from '../../assets/proaktif.svg';
import {ReactComponent as Birokratif} from '../../assets/birokratif.svg';
import {ReactComponent as Reaktif} from '../../assets/reaktif.svg';
import {ReactComponent as Patologik} from '../../assets/patologik.svg';

const style = {
  width: '100%',
  height: 'auto'
}

const Illustration = ({tipe}) => {
  switch (tipe) {
    case 'generatif':
      return <Generatif style={style} />
    case 'proaktif':
      return <Proaktif style={style} />
    case 'birokratif':
      return <Birokratif style={style} />
    case 'reaktif':
      return <Reaktif style={style} />
    case 'patologik':
      return <Patologik style={style} />
    default:
      return null
  }
}

export default Illustration