import React from 'react';
import { tipeColor } from '../../utils/tipe';

const ColorInfo = ({tipe}) => {
  const color = tipeColor[tipe];
  return (
    <div style={{ display: 'flex', padding: '0.5rem' }}>
      <div 
        style={{
          width: 25,
          height: 22,
          backgroundColor: color,
          marginRight: '0.4rem'
        }}
      />
      <span style={{ textTransform: 'capitalize' }}>{` = ${tipe}`}</span>
    </div>
  );
}

export default ColorInfo;