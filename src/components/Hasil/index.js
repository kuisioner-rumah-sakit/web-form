import React, { useState } from 'react';
import { capitalize, Grid, Paper, withStyles } from '@material-ui/core';
import Illustration from './illustration';
import { Rating } from '@material-ui/lab';
import ColorInfo from './ColorInfo';
import getTipe, { getStar, tipeColor } from '../../utils/tipe';
import CustomTab from '../CustomTab';
import CustomTabs from '../CustomTabs';
import { getBottom3 } from '../../calculation';
import resultDesc from '../../resource/resultDesc.json';
import { isNetralFactor } from '../../utils/factor';

const styles = () => ({
  root: {
    padding: '1.5rem 1.5rem 1rem'
  },
  title: {
    textShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
    fontWeight: 500,
    textAlign: 'center',
    margin: 0,
    width: "100%"
  },
  grid: {
    marginTop: '1rem'
  },
  paper: {
    borderRadius: 30,
    maxWidth: 900,
    margin: 'auto'
  },
  infoContainer: {
    maxWidth: 900,
    margin: '0.67rem auto'
  },
  tabs: {
    maxWidth: 900,
  },
  arti: {
    fontWeight: 600,
  },
  li: {
    margin: '0.5rem',
    fontWeight: 300
  }
});

const Hasil = ({result, classes}) => {
  const tipe = getTipe(result.maturitas);
  const star = getStar(result.maturitas);
  const [tab, setTab] = useState(0);
  const {
    // hasil
    // iklim Keselamatan
    kerjasama,
    komunikasi,
    lingkungan,
    pelatihan,
    pelaporan,
    pembelajaran,
    // situasional
    regulasi,
    kepemimpinan,
    risiko,
    // perilaku Keselamatan
    kepatuhan,
    partisipasi,
    mengganggu,
    // maturitas
    mutu,
    pasien,
    pekerja,
  } = result;

  const terlemah = getBottom3({
    kerjasama,
    komunikasi,
    lingkungan,
    pelatihan,
    pelaporan,
    pembelajaran,
    // situasional
    regulasi,
    kepemimpinan,
    risiko,
    // perilaku Keselamatan
    kepatuhan,
    partisipasi,
    mengganggu,
    // maturitas
    mutu,
    pasien,
    pekerja,
  });

  return (
    <>
      <CustomTabs
        value={tab}
        indicatorColor="primary"
        textColor="primary"
        aria-label="tabs"
        onChange={(_e, val)=>setTab(val)}
      >
        <CustomTab label="Hasil Tingkat Kematangan"></CustomTab>
        <CustomTab label="Indikator Terlemah"></CustomTab>
      </CustomTabs>
      <Paper className={classes.paper}>
        
        {tab === 0 && (
          <Grid container className={classes.root}>
            <h2 className={classes.title}>
              Hasil Kematangan Budaya Keselamatan Rumah Sakit Anda tingkat
            </h2> <br/>
            <h2 className={classes.title}> {capitalize(tipe)} </h2>
            <Grid item xs={12} style={{textAlign: 'center'}} className={classes.grid}>
              <Rating max={5} readOnly value={star} />
            </Grid>
            <Grid container item className={classes.grid}>
              <Grid item sm={6} xs={12}>
                <Illustration tipe={tipe} />
              </Grid>
              <Grid item sm={6} xs={12}>
                <p className={classes.arti}>Tingkat {capitalize(tipe)} artinya:</p>
                <ol>
                  {resultDesc[tipe].map(desc => (
                      <li className={classes.li}>{desc}</li>
                    ))
                  }
                </ol>
              </Grid>
            </Grid>
          </Grid>
        )}

        {tab === 1 && (
          <Grid container className={classes.root}>
            <h2 className={classes.title} style={{marginBottom: "2rem"}}>
              Indikator Terlemah Rumah Sakit yang Perlu Ditingkatkan.
            </h2>
            <Grid container item className={classes.grid}>
              {terlemah.map(x=>{
                return (
                  <Grid item container xs={12}>
                    <Grid item xs={8}>
                      <h3>{x.nama}</h3>
                    </Grid>
                    <Grid item xs={4} container alignItems="center">
                      <span
                        style={{
                          backgroundColor: isNetralFactor(x.nama) ? tipeColor.netral : tipeColor[getTipe(x.avg)],
                          borderRadius: "50px",
                          textAlign: "center",
                          padding: "0.5rem 0.67rem",
                          color: '#fff',
                          minWidth: "4rem"
                        }}
                      >
                        {Math.round(x.avg*10)/10}
                      </span>
                    </Grid>
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
        )}

      </Paper>
      <Grid container justify="space-between" className={classes.infoContainer}>
        <ColorInfo tipe="patologik" />
        <ColorInfo tipe="reaktif" />
        <ColorInfo tipe="birokratif" />
        <ColorInfo tipe="proaktif" />
        <ColorInfo tipe="generatif" />
      </Grid>
    </>
  );
}

export default withStyles(styles)(Hasil);
