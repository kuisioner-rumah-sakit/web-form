import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiDialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { capitalize, Grid } from '@material-ui/core';
import getTipe from '../../utils/tipe';
import factors from '../../resource/factors.json';
import ResultItem from './ResultItem';
import ModalPerbaikan from '../ModalPerbaikan';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other} style={{padding: '1.5rem 1rem 0.5rem'}}>
      <Typography variant="h6" style={{fontWeight: 600}}>{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(() => ({
  root: {
    padding: '1rem 1rem 3rem 1rem',
    fontSize: '1.2rem',
    fontWeight: 300,
    textAlign: "center"
  },
}))(MuiDialogContent);

const Dialog = withStyles(() => ({
  paper: {
    borderRadius: 40
  },
}))(MuiDialog);

export default function ResultDialogs({open, handleClose, result, variabel}) {
  const lowestValue = Math.min(...Object.values(result));
  const lowestFactor = () => {
    for (let indikator of Object.keys(result)) {
      // console.log(indikator,  lowestValue)
      if (result[indikator] === lowestValue) return factors[indikator].name;
    }
    return "Tidak Ada";
  }
  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="Result-dialog-title"
      open={open}
      fullWidth
      maxWidth="xs"
    >
      <DialogTitle id="Result-dialog-title" onClose={handleClose}>
        <Grid container>
          <Grid item xs={7} container justify="center">
            <span>Indikator</span>
          </Grid>
          <Grid item xs={5} container justify="center">
            <span>Nilai</span>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent dividers>
        {Object.keys(result).map(indikator => {
          const tipe = getTipe(result[indikator]);
          return (
            <>
              <ModalPerbaikan factorId={indikator} />
              <ResultItem 
                factor={capitalize(factors[indikator].name)}
                value={Math.round(result[indikator] * 10)/10}
                tipe={tipe}
              />
            </>
          )})}
        <Grid item xs={12}>
          <h2 style={{fontWeight: 300, textAlign: "center", width: "100%", margin: "2rem auto 0"}}>
            Indikator Terlemah dari Variabel <span style={{fontWeight: 500}}>{variabel} </span>
            adalah <span style={{fontWeight: 500}}>{lowestFactor()}</span>
          </h2>
        </Grid> 
      </DialogContent>
    </Dialog>
  );
}