import { capitalize, Grid } from '@material-ui/core';
import React from 'react';
import { tipeColor } from '../../utils/tipe';
import { Accordion, AccordionSummary, AccordionDetails } from '../ResultBar/accordion';

const ResultItem = ({tipe, factor, value}) => {
  // const [expanded, setExpand] = useState(false);
  return (
    <Accordion
      expanded={false}
      onChange={() => {}}
      style={{marginBottom: '1rem'}}
    >
      <AccordionSummary>
        <Grid container>
          <Grid item xs={7} container justify="center" alignItems="center">
            <p style={{margin: '0.67rem'}}>{factor}</p>
          </Grid>
          <Grid item xs={5} container justify="center" alignItems="center">
            <p 
              style={{
                backgroundColor: tipeColor.netral,
                color: 'white',
                padding: '0.5rem 2rem',
                minWidth: '2.33rem',
                borderRadius: '100px',
                margin: '0'
              }}
            >{value}</p>
          </Grid>
        </Grid>
      </AccordionSummary>
      <AccordionDetails>
        <div style={{textAlign: "left", fontSize: '0.8em', fontWeight: 400}}>
          Indikator {factor} berada pada tingkat kematangan {capitalize(tipe)}
        </div>
      </AccordionDetails>
    </Accordion>
  )
}

export default ResultItem;