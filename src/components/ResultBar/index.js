import React, { useState } from 'react';
import { Grid, capitalize } from '@material-ui/core';
import {ReactComponent as Triangle} from '../../assets/triangle.svg';
import getTipe, { tipeColor } from '../../utils/tipe';
import { Accordion, AccordionSummary, AccordionDetails } from './accordion';
import { isNetralFactor } from '../../utils/factor';

const ResultBar = ({factor, value, openDialog}) => {
  const tipe = getTipe(value);
  const color = isNetralFactor(factor) ? tipeColor.netral : tipeColor[tipe];
  const [expanded, setExpand] = useState(false);
  return (
    <Grid item xs={12} style={{ margin: openDialog ? "1rem 0" : "0 0 1rem"}}>
      <Accordion expanded={expanded} onChange={ !!openDialog ? () => openDialog() : () => setExpand(!expanded)}>
        <AccordionSummary>
          <h4 
            style={{margin: "0.33rem 0", display: "flex", alignItems: "center"}}
            onClick={openDialog}
          >
            {factor}
            {openDialog && 
              <Triangle fill="#C4C4C4" 
                style={{
                  transform: "rotate(90deg) scale(0.8)",
                  marginLeft: "0.5rem"
                }}
              />
            }
          </h4>
          <div 
            style={{
              position: "relative",
              height: 40,
              width: "100%",
              backgroundColor: "#CFCFCF",
              borderRadius: 16,
              marginBottom: "2rem"
            }}
          >
            <div 
              style ={{
                position: "absolute",
                backgroundColor: color,
                borderRadius: 16,
                height: 40,
                width: `${value*10}%`
              }}
            />
            <span
              style={{
                position: "absolute",
                left: `calc(${value*10}% - 0.7rem)`,
                top: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                fontWeight: 400
              }}
            >
              <Triangle fill={color} />
              <span style={{marginTop: 6}}>
                {Math.round(value*10)/10}
              </span>
            </span>
          </div>
        </AccordionSummary>
        <AccordionDetails>
          <div>Indikator {factor} berada pada tingkat kematangan {capitalize(tipe)}</div>
        </AccordionDetails>
      </Accordion>
    </Grid>
  );
}

export default ResultBar;