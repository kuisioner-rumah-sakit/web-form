import React from 'react';
import { Grid, withStyles } from '@material-ui/core';
import { Skeleton, ToggleButton, ToggleButtonGroup } from '@material-ui/lab';

const style = () => ({
  root: {
    borderRadius: "10px",
    padding: "1rem",
    margin: "1rem 0",
    fontWeight: "400",
    color: "#111",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)",
  },
  buttonItem: {
    width: "70px",
    color: "black",
    fontWeight: "bold",
    fontSize: "1rem",
    '&.Mui-selected': {
      backgroundColor: "#969696"
    }
  },
  buttonGroup: {
    backgroundColor: "white",
    // height: 60
  }
});

const Soal = ({classes, pertanyaan, jawaban, index, setJawaban, dummy}) => {
  return (
    <>
    {dummy ? (
      <Grid item container sm={12} className={classes.root} style={{backgroundColor: index%2 ? "#C4C4C4" : "#6AC9DB"}}>
        <Grid item xs={12} sm={8}>
          <Skeleton animation="wave" width="100%" />
          <Skeleton animation="wave" width="80%" />
          <Skeleton animation="wave" width="90%" />
        </Grid>
        <Grid item xs={12} sm={4} container justify="flex-end" alignItems="center">
          <Grid item xs={12} sm="auto">
            <Skeleton animation="wave" width={210} height={58} style={{transform: "scale(1)", margin: "auto"}} />
          </Grid>
        </Grid>
      </Grid>
      ) : (
      <Grid item container sm={12} className={classes.root} style={{backgroundColor: index%2 ? "#C4C4C4" : "#6AC9DB"}}>
        <Grid item xs={12} sm={8}>
          <p>{pertanyaan}</p>
        </Grid>
        <Grid item xs={12} sm={4} container justify="flex-end" alignItems="center">
          <Grid item xs={12} sm="auto" style={{textAlign: "center"}}>
            <ToggleButtonGroup
              className={classes.buttonGroup}
              size="large"
              exclusive
              value={jawaban}
              onChange={(_e, val)=>setJawaban(index, val)}
            >
              <ToggleButton className={classes.buttonItem} value={0}>0</ToggleButton>
              <ToggleButton className={classes.buttonItem} value={5}>5</ToggleButton>
              <ToggleButton className={classes.buttonItem} value={10}>10</ToggleButton>
            </ToggleButtonGroup>
          </Grid>
        </Grid>
      </Grid>
    )}
  </>
  )
};

export default withStyles(style)(Soal);