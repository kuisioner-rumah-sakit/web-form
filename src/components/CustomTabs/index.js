import React from 'react';
import { Tabs, withStyles } from '@material-ui/core';

const CustomTabs = withStyles({
  indicator: {
    backgroundColor: 'transparent',
  },
  root: {
    padding: '0 30px',
    maxWidth: "calc(900px - 2 * 30px)",
    margin: "0 auto",
  }
})((props) => <Tabs disableRipple {...props}/>);

export default CustomTabs;